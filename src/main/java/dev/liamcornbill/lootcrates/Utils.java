package dev.liamcornbill.lootcrates;

import dev.liamcornbill.liamlib.CC;
import org.bukkit.entity.Player;

public class Utils {
    public static final String prefix = CC.GOLD + CC.BOLD + "Loot Crates" + CC.DGRAY + CC.BOLD + " » " + CC.YELLOW;

    public static void sendMsg(Player player, String msg) {
        player.sendMessage(prefix + msg);
    }
}
