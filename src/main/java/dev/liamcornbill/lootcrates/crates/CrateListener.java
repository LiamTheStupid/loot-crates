package dev.liamcornbill.lootcrates.crates;

import dev.liamcornbill.liamlib.CC;
import dev.liamcornbill.liamlib.LocationSerializable;
import dev.liamcornbill.liamlib.gui.GuiManager;
import dev.liamcornbill.liamlib.gui.events.PlayerLeaveGuiSessionEvent;
import dev.liamcornbill.lootcrates.Utils;
import dev.liamcornbill.lootcrates.gui.GuiCrate;
import dev.liamcornbill.lootcrates.gui.GuiList;
import dev.liamcornbill.lootcrates.links.Link;
import dev.liamcornbill.lootcrates.links.LinkManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

public class CrateListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getHand() == EquipmentSlot.HAND) {
            Block block = event.getClickedBlock();

            if (block != null && (block.getType() == Material.CHEST || block.getType() == Material.TRAPPED_CHEST
                    || block.getType() == Material.ENDER_CHEST)) {
                if (LinkManager.isLinking(player)) {
                    Crate crate = CrateManager.getCrate(LinkManager.getLinkingCrate(player));

                    if (crate != null) {
                        event.setCancelled(true);
                        LinkManager.create(new LocationSerializable(block.getLocation()), crate.getId());
                        LinkManager.stopLinking(player);
                        Utils.sendMsg(player, CC.GREEN + "You have linked this chest to " + CC.translate(crate.getName()) + " successfully.");
                    }
                } else if (LinkManager.isUnlinking(player)) {
                    Crate crate = CrateManager.getCrate(LinkManager.getUnlinkingCrate(player));

                    if (crate != null) {
                        event.setCancelled(true);
                        LinkManager.remove(new LocationSerializable(block.getLocation()));
                        LinkManager.stopUnlinking(player);
                        Utils.sendMsg(player, CC.GREEN + "You have unlinked this chest successfully.");
                    }
                } else {
                    Link link = LinkManager.getLink(new LocationSerializable(block.getLocation()));

                    if (link != null) {
                        Crate crate = CrateManager.getCrate(link.getCrateId());

                        if (crate != null) {
                            event.setCancelled(true);

                            if (player.isSneaking()) {
                                GuiManager.startSession(new GuiList(player, crate), player);
                            } else {
                                GuiManager.startSession(new GuiCrate(player, crate), player);
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onGuiSessionLeave(PlayerLeaveGuiSessionEvent event) {
        if (event.getSession().getGui() instanceof GuiCrate gui) {
            if (!gui.isFinished()) {
                event.setCancelled(true);
            }
        }
    }
}
