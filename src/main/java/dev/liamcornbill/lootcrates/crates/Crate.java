package dev.liamcornbill.lootcrates.crates;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import dev.liamcornbill.liamlib.LocationSerializable;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class Crate implements ConfigurationSerializable {
    private final String id;
    private final String name;
    private final HashMap<ItemStack, Double> items;

    public Crate(String id, String name) {
        this(id, name, new HashMap<>());
    }

    public Crate(String id, String name, HashMap<ItemStack, Double> items) {
        this.id = id;
        this.name = name;
        this.items = items;
    }

    public Crate(Map<String, Object> map) {
        this.id = (String) map.get("id");
        this.name = (String) map.get("name");
        this.items = (HashMap<ItemStack, Double>) map.get("items");
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addItem(ItemStack stack, double chance) {
        items.put(stack, chance);
    }

    public void removeItem(ItemStack stack) {
        items.remove(stack);
    }

    public HashMap<ItemStack, Double> getItems() {
        return items;
    }

    @Override
    public Map<String, Object> serialize() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("id", id);
        result.put("name", name);
        result.put("items", items);

        return result;
    }
}
