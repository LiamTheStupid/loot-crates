package dev.liamcornbill.lootcrates.crates;

import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.database.CrateDao;
import dev.liamcornbill.lootcrates.links.LinkManager;
import dev.liamcornbill.lootcrates.storage.CrateStorage;

import java.util.HashMap;

public class CrateManager {
    private static HashMap<String, Crate> crates = new HashMap<>();

    public static void loadAll() {
        if (LootCrates.mysqlEnabled()) {
            crates = CrateDao.load();
        } else {
            crates = CrateStorage.load();
        }
    }

    public static void create(String id, String name) {
        Crate crate = new Crate(id, name);
        crates.put(id, crate);

        if (LootCrates.mysqlEnabled()) {
            CrateDao.create(crate);
        } else {
            CrateStorage.save(crates);
        }
    }

    public static void delete(Crate crate) {
        crates.remove(crate.getId());
        LinkManager.removeFromCrate(crate.getId());

        if (LootCrates.mysqlEnabled()) {
            CrateDao.delete(crate);
        } else {
            CrateStorage.save(crates);
        }
    }

    public static void save(Crate crate) {
        if (LootCrates.mysqlEnabled()) {
            CrateDao.update(crate);
        } else {
            CrateStorage.save(crates);
        }
    }

    public static Crate getCrate(String id) {
        return crates.get(id);
    }

    public static boolean exists(String id) {
        return getCrate(id) != null;
    }
}
