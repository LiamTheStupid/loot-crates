package dev.liamcornbill.lootcrates.database;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import dev.liamcornbill.liamlib.LocationSerializable;
import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.links.Link;

import javax.xml.stream.Location;
import java.sql.*;
import java.util.HashMap;

public class LinkDao {
    public static void init() {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("CREATE TABLE IF NOT EXISTS Links(location VARCHAR(150), crate VARCHAR(50), PRIMARY KEY (location));");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<LocationSerializable, Link> load() {
        HashMap<LocationSerializable, Link> links = new HashMap<>();

        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM Links")) {
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        LocationSerializable location = LootCrates.getGson().fromJson(result.getString("location"), LocationSerializable.class);
                        String crate = result.getString("crate");
                        links.put(location, new Link(location, crate));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return links;
    }

    public static void create(Link link) {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO Links (location,crate) VALUES(?,?);")) {
                statement.setString(1, LootCrates.getGson().toJson(link.getLocation()));
                statement.setString(2, link.getCrateId());
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void remove(LocationSerializable location) {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM Links WHERE location=?;")) {
                statement.setString(1, new Gson().toJson(LootCrates.getGson().toJson(location)));
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
