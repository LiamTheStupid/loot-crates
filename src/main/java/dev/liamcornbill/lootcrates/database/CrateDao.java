package dev.liamcornbill.lootcrates.database;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.crates.Crate;

import java.sql.*;
import java.util.HashMap;

public class CrateDao {
    public static void init() {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("CREATE TABLE IF NOT EXISTS Crates(id VARCHAR(50), crate LONGTEXT, PRIMARY KEY (id));");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static HashMap<String, Crate> load() {
        HashMap<String, Crate> crates = new HashMap<>();

        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM Crates")) {
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        String id = result.getString("id");
                        String crate = result.getString("crate");
                        crates.put(id, LootCrates.getGson().fromJson(crate, Crate.class));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return crates;
    }

    public static void create(Crate crate) {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO Crates (id,crate) VALUES(?,?);")) {
                statement.setString(1, crate.getId());
                statement.setString(2, LootCrates.getGson().toJson(crate));
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void update(Crate crate) {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO Crates (id,crate) VALUES(?,?) ON DUPLICATE KEY UPDATE crate=?;")) {
                statement.setString(1, crate.getId());
                String serializedCrate = LootCrates.getGson().toJson(crate);
                statement.setString(2, serializedCrate);
                statement.setString(3, serializedCrate);
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void delete(Crate crate) {
        try (Connection connection = LootCrates.getDatabase().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM Crates WHERE id=?;")) {
                statement.setString(1, crate.getId());
                statement.executeUpdate();
            }

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM Links WHERE crate=?;")) {
                statement.setString(1, crate.getId());
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
