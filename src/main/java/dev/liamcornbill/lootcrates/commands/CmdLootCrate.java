package dev.liamcornbill.lootcrates.commands;

import dev.liamcornbill.liamlib.CC;
import dev.liamcornbill.liamlib.command.Command;
import dev.liamcornbill.liamlib.gui.GuiManager;
import dev.liamcornbill.lootcrates.Utils;
import dev.liamcornbill.lootcrates.crates.Crate;
import dev.liamcornbill.lootcrates.crates.CrateManager;
import dev.liamcornbill.lootcrates.gui.GuiList;
import dev.liamcornbill.lootcrates.links.LinkManager;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CmdLootCrate extends Command {
    private static final List<String> subCommands = Arrays.asList("new", "delete", "add", "list", "link", "unlink");

    public CmdLootCrate() {
        super ("lootcrates", new String[]{ "lootcrate", "lc" }, "lootcrates.admin");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof Player player) {
            if (args.length == 0) {
                Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate [new | add | list | link]");
            } else {
                String subCommand = args[0];
                String[] subArgs = Arrays.stream(args, 1, args.length).toArray(String[]::new);

                switch (subCommand.toLowerCase()) {
                    case "new" -> subNew(player, subArgs);
                    case "delete" -> subDelete(player, subArgs);
                    case "add" -> subAdd(player, subArgs);
                    case "list" -> subList(player, subArgs);
                    case "link" -> subLink(player, subArgs);
                    case "unlink" -> subUnlink(player, subArgs);
                    default -> Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate [new | add | list | link]");
                }
            }
        } else {
            sender.sendMessage("You have to be a player to execute that command.");
        }
    }

    private void subNew(Player player, String[] args) {
        if (args.length >= 2) {
            String id = args[0];
            String name = String.join(" ", Arrays.stream(args, 1, args.length).toArray(String[]::new));

            if (CrateManager.exists(id)) {
                Utils.sendMsg(player, CC.RED + "A crate with that ID already exists!");
                return;
            }

            CrateManager.create(id, name);
            Utils.sendMsg(player, CC.GREEN + "Crate " + CC.translate(name) + CC.GREEN + " has been created.");
            Utils.sendMsg(player, CC.YELLOW + "To add items to this crate, hold the item and type /lootcrate add [id] [chance%]");
        } else {
            Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate new [id] [display name]");
        }
    }

    private void subDelete(Player player, String[] args) {
        if (args.length >= 1) {
            String id = args[0];

            if (!CrateManager.exists(id)) {
                Utils.sendMsg(player, CC.RED + "A crate with that ID doesnt exists!");
                return;
            }

            Crate crate = CrateManager.getCrate(id);
            CrateManager.delete(crate);
            Utils.sendMsg(player, CC.GREEN + "Crate " + CC.translate(crate.getName()) + CC.GREEN + " has been deleted.");
        } else {
            Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate delete [id]");
        }
    }

    private void subAdd(Player player, String[] args) {
        if (args.length >= 2) {
            String id = args[0];
            double chance = -1;

            if (!CrateManager.exists(id)) {
                Utils.sendMsg(player, CC.RED + "There is no crate with that ID");
                return;
            }

            try {
                chance = Double.parseDouble(args[1].endsWith("%") ? args[1].substring(0, args[1].length() - 1) : args[1]);
            } catch (Exception e) {
                Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate add [id] [chance%]");
                return;
            }

            ItemStack item = player.getInventory().getItemInMainHand();

            if (item.getType() == Material.AIR) {
                Utils.sendMsg(player, CC.GRAY + "Please hold the item you wish to add in your main hand");
                return;
            }

            Crate crate = CrateManager.getCrate(id);
            crate.addItem(new ItemStack(item), chance);
            CrateManager.save(crate);
            Utils.sendMsg(player, CC.GREEN + "Item " + CC.DGREEN + item.getType().toString().toLowerCase().replaceAll("_", " ") +
                    CC.GREEN + " has been added to crate " + CC.translate(crate.getName()));
        } else {
            Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate add [id] [chance%]");
        }
    }

    private void subList(Player player, String[] args) {
        if (args.length >= 1) {
            String id = args[0];

            if (!CrateManager.exists(id)) {
                Utils.sendMsg(player, CC.RED + "There is no crate with that ID");
                return;
            }

            Crate crate = CrateManager.getCrate(id);
            GuiManager.startSession(new GuiList(player, crate), player);
        } else {
            Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate list [id]");
        }
    }

    private void subLink(Player player, String[] args) {
        if (args.length >= 1) {
            String id = args[0];

            if (!CrateManager.exists(id)) {
                Utils.sendMsg(player, CC.RED + "There is no crate with that ID");
                return;
            }

            LinkManager.startLinking(player, id);
            Utils.sendMsg(player, CC.GREEN + "Please right click on the chest to link to this crate.");
        } else {
            Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate link [id]");
        }
    }

    private void subUnlink(Player player, String[] args) {
        if (args.length >= 1) {
            String id = args[0];

            if (!CrateManager.exists(id)) {
                Utils.sendMsg(player, CC.RED + "There is no crate with that ID");
                return;
            }

            LinkManager.startUnlinking(player, id);
            Utils.sendMsg(player, CC.GREEN + "Please right click on the chest to unlink.");
        } else {
            Utils.sendMsg(player, CC.GRAY + "Usage: /lootcrate unlink [id]");
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) {
        if (args.length == 1) {
            return StringUtil.copyPartialMatches(args[0], subCommands, new ArrayList<>());
        }

        return new ArrayList<>();
    }
}
