package dev.liamcornbill.lootcrates.gui;

import dev.liamcornbill.liamlib.CC;
import dev.liamcornbill.liamlib.gui.Gui;
import dev.liamcornbill.liamlib.gui.GuiItem;
import dev.liamcornbill.liamlib.gui.GuiManager;
import dev.liamcornbill.liamlib.gui.ItemBuilder;
import dev.liamcornbill.lootcrates.Utils;
import dev.liamcornbill.lootcrates.crates.Crate;
import dev.liamcornbill.lootcrates.crates.CrateManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class GuiList extends Gui {
    private final Player player;
    private final Crate crate;

    public GuiList(Player player, Crate crate) {
        super(6);

        this.player = player;
        this.crate = crate;
    }

    @Override
    public void init() {
        int totalItems = crate.getItems().size();
        int pages = totalItems > 45 && totalItems % 45 > 0 ? totalItems / 45 + 1 : totalItems / 45;

        for (int page=0; page<=pages; page++) {
            List<GuiItem> items = new ArrayList<>();

            for (int i=0; i<45; i++) {
                if (45*page + i < totalItems) {
                    Map.Entry<ItemStack, Double> data = (Map.Entry<ItemStack, Double>) crate.getItems().entrySet().toArray()[45*page + i];

                    List<String> lore = new ArrayList<>();

                    lore.add(" ");
                    lore.add(CC.YELLOW + data.getValue() + "%");

                    if (player.hasPermission("lootcrates.admin")) {
                        lore.add(" ");
                        lore.add(CC.RED + "Click to Remove");
                    }

                    ItemStack stack = new ItemBuilder(new ItemStack(data.getKey())).lore(lore).finish();

                    if (player.hasPermission("lootcrates.admin")) {
                        items.add(new GuiItem(i, stack, (inv, player) -> {
                            crate.removeItem(data.getKey());
                            CrateManager.save(crate);
                            player.closeInventory();

                            Utils.sendMsg(player, CC.DRED + data.getKey().getType().toString().toLowerCase().replaceAll("_", " ") + CC.RED + " removed from crate " + CC.translate(crate.getName()));
                        }));
                    } else {
                        items.add(new GuiItem(i, stack));
                    }
                }
            }

            int finalPage = page;

            if (page > 0) {
                items.add(new GuiItem(45, new ItemBuilder(Material.PAPER).name(CC.YELLOW + "← Last Page").finish(),
                        (inv, player) -> {
                            setActiveScreen(finalPage -1);
                            GuiManager.refreshInventory(inv);
                        }));
            }

            if (page < pages - 1) {
                items.add(new GuiItem(53, new ItemBuilder(Material.PAPER).name(CC.YELLOW + "Next Page →").finish(),
                        (inv, player) -> {
                            setActiveScreen(finalPage + 1);
                            GuiManager.refreshInventory(inv);
                        }));
            }

            addScreen(page, "Crate Preview | Page " + (page + 1), items.toArray(GuiItem[]::new));
        }
    }
}
