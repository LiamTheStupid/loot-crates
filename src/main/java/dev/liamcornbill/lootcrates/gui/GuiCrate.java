package dev.liamcornbill.lootcrates.gui;

import dev.liamcornbill.liamlib.CC;
import dev.liamcornbill.liamlib.gui.Gui;
import dev.liamcornbill.liamlib.gui.GuiItem;
import dev.liamcornbill.liamlib.gui.GuiManager;
import dev.liamcornbill.liamlib.gui.ItemBuilder;
import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.crates.Crate;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class GuiCrate extends Gui {
    private final Player player;
    private final Crate crate;
    private int taskId;

    public List<ItemStack> rewards = new ArrayList<>();
    private boolean finished = false;

    private int totalTicks = 0;
    private int ticksPerUpdate = 1;
    private final int openingDuration = 250;
    private final int waitDuration = 60;

    public GuiCrate(Player player, Crate crate) {
        super (3);

        this.player = player;
        this.crate = crate;

        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(LootCrates.getInstance(), () -> {
            if (totalTicks < openingDuration && totalTicks % ticksPerUpdate == 0) {
                if (rewards.size() == 0) {
                    for (int i=0; i<7; i++) {
                        rewards.add(getReward());
                    }
                } else {
                    rewards.remove(0);
                    rewards.add(getReward());
                }

                GuiManager.refreshSession(player);
                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 1);

            }

            totalTicks++;

            if (totalTicks > 30 && totalTicks < 100) {
                ticksPerUpdate = 2;
            } else if (totalTicks > 100 && totalTicks < 160) {
                ticksPerUpdate = 3;
            }  else if (totalTicks > 160 && totalTicks < 200) {
                ticksPerUpdate = 5;
            } else if (totalTicks > 200 && totalTicks < openingDuration) {
                ticksPerUpdate = 7;
            } else if (totalTicks == openingDuration) {
                player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                finished = true;
                GuiManager.refreshSession(player);
            }

            if (totalTicks >= openingDuration + waitDuration) {
                Bukkit.getScheduler().cancelTask(taskId);
                player.closeInventory();
            }
        }, 1, 1);
    }

    @Override
    public void init() {
        List<GuiItem> items = new ArrayList<>();

        if (finished) {
            for (int i=0; i<27; i++) {
                if (i == 13) {
                    items.add(new GuiItem(i, new ItemBuilder(rewards.get(3)).finish()));
                } else {
                    items.add(new GuiItem(i, new ItemBuilder(Material.LIME_STAINED_GLASS_PANE).name(" ").finish()));
                }
            }
        } else {
            int[] filler = new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 17, 18, 19, 20, 21, 23, 24, 25, 26 };
            Arrays.stream(filler).forEach(x -> items.add(new GuiItem(x,new ItemBuilder(Material.LIGHT_BLUE_STAINED_GLASS_PANE).name(" ").finish(), (inv, player) -> {
                if (rewards.size() == 0) {
                    for (int i=0; i<7; i++) {
                        rewards.add(getReward());
                    }
                } else {
                    Collections.rotate(rewards, 1);
                    rewards.remove(0);
                    rewards.add(getReward());
                }
                GuiManager.refreshInventory(inv);
            })));

            ItemStack torch = new ItemBuilder(Material.REDSTONE_TORCH).name(CC.GOLD + CC.MAGIC + "---" + CC.GOLD + CC.BOLD + " Reward " + CC.MAGIC + "---").finish();
            items.add(new GuiItem(4, torch));
            items.add(new GuiItem(22, torch));

            for (int i=0; i<=6; i++) {
                if (rewards.size() > i) {
                    items.add(new GuiItem(10 + i, rewards.get(i)));
                }
            }
        }

        addScreen(0, CC.translate(crate.getName()), items.toArray(GuiItem[]::new));
    }

    @Override
    public void onTerminate() {
        if (taskId != -1) {
            Bukkit.getScheduler().cancelTask(taskId);
        }

        player.getInventory().addItem(rewards.get(3));
    }

    private ItemStack getReward() {
        ItemStack selected = null;

        while (selected == null) {
            ItemStack item = crate.getItems().keySet().stream().toList().get(ThreadLocalRandom.current().nextInt(0, crate.getItems().size()));
            double chance = crate.getItems().get(item);

            if (ThreadLocalRandom.current().nextDouble(0, 100) <= chance) {
                selected = item;
            }
        }

        return selected;
    }

    public boolean isFinished() {
        return finished;
    }
}
