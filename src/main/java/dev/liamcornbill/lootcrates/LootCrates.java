package dev.liamcornbill.lootcrates;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.liamcornbill.liamlib.LocationSerializable;
import dev.liamcornbill.liamlib.database.Database;
import dev.liamcornbill.liamlib.gui.GuiListener;
import dev.liamcornbill.liamlib.gui.GuiManager;
import dev.liamcornbill.lootcrates.commands.CmdLootCrate;
import dev.liamcornbill.lootcrates.crates.Crate;
import dev.liamcornbill.lootcrates.crates.CrateListener;
import dev.liamcornbill.lootcrates.crates.CrateManager;
import dev.liamcornbill.lootcrates.database.CrateDao;
import dev.liamcornbill.lootcrates.database.LinkDao;
import dev.liamcornbill.lootcrates.links.LinkManager;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

public class LootCrates extends JavaPlugin {
    private static final Gson gson;

    static {
        gson = new GsonBuilder().enableComplexMapKeySerialization().setPrettyPrinting().serializeNulls().create();
    }

    static {
        ConfigurationSerialization.registerClass(LocationSerializable.class);
        ConfigurationSerialization.registerClass(Crate.class);
    }

    private static LootCrates instance;
    private static Database db;

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();

        boolean enabled = getConfig().getBoolean("mysql.enabled");
        String host = getConfig().getString("mysql.host");
        int port = getConfig().getInt("mysql.port");
        String database = getConfig().getString("mysql.database");
        String username = getConfig().getString("mysql.username");
        String password = getConfig().getString("mysql.password");

        if (enabled) {
            db = new Database(host, port, database, username, password);
            db.connect();

            CrateDao.init();
            LinkDao.init();
        }

        CrateManager.loadAll();
        LinkManager.loadAll();

        GuiManager.init(this);

        getServer().getPluginManager().registerEvents(new CrateListener(), this);

        new CmdLootCrate();
    }

    @Override
    public void onDisable() {
        if (db != null) {
            db.disconnect();
        }
    }

    public static Database getDatabase() {
        return db;
    }

    public static boolean mysqlEnabled() {
        return db != null;
    }

    public static LootCrates getInstance() {
        return instance;
    }

    public static Gson getGson() {
        return gson;
    }
}
