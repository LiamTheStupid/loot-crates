package dev.liamcornbill.lootcrates.links;

import dev.liamcornbill.liamlib.LocationSerializable;
import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.database.LinkDao;
import dev.liamcornbill.lootcrates.storage.LinkStorage;
import org.bukkit.entity.Player;

import java.util.*;

public class LinkManager {
    private static HashMap<LocationSerializable, Link> links = new HashMap<>();
    private static HashMap<UUID, String> linking = new HashMap<>();
    private static HashMap<UUID, String> unlinking = new HashMap<>();

    public static void loadAll() {
        if (LootCrates.mysqlEnabled()) {
            links = LinkDao.load();
        } else {
            links = LinkStorage.load();
        }
    }

    public static void create(LocationSerializable location, String crateId) {
        Link link = new Link(location, crateId);
        links.put(location, link);

        if (LootCrates.mysqlEnabled()) {
            LinkDao.create(link);
        } else {
            LinkStorage.save(links);
        }
    }

    public static void remove(LocationSerializable location) {
        if (links.containsKey(location)) {
            links.remove(location);

            if (LootCrates.mysqlEnabled()) {
                LinkDao.remove(location);
            } else {
                LinkStorage.save(links);
            }
        }
    }

    public static void removeFromCrate(String crateId) {
        List<LocationSerializable> remove = new ArrayList<>();

        links.forEach((location, link) -> {
            if (link.getCrateId().equalsIgnoreCase(crateId)) {
                remove.add(location);
            }
        });

        remove.forEach(location -> links.remove(location));

        if (!LootCrates.mysqlEnabled()) {
            LinkStorage.save(links);
        }
    }

    public static Link getLink(LocationSerializable location) {
        for (Map.Entry<LocationSerializable, Link> entry: links.entrySet()) {
            if (entry.getKey().getWorld().equalsIgnoreCase(location.getWorld()) && entry.getKey().getX() == location.getX()
                    && entry.getKey().getY() == location.getY() && entry.getKey().getZ() == location.getZ()) {
                return entry.getValue();
            }
        }

        return null;
    }

    public static boolean exists(LocationSerializable location) {
        return links.containsKey(location);
    }

    public static void startLinking(Player player, String id) {
        linking.put(player.getUniqueId(), id);
    }

    public static boolean isLinking(Player player) {
        return linking.containsKey(player.getUniqueId());
    }

    public static String getLinkingCrate(Player player) {
        return linking.get(player.getUniqueId());
    }

    public static void stopLinking(Player player) {
        linking.remove(player.getUniqueId());
    }

    public static void startUnlinking(Player player, String id) {
        unlinking.put(player.getUniqueId(), id);
    }

    public static boolean isUnlinking(Player player) {
        return unlinking.containsKey(player.getUniqueId());
    }

    public static String getUnlinkingCrate(Player player) {
        return unlinking.get(player.getUniqueId());
    }

    public static void stopUnlinking(Player player) {
        unlinking.remove(player.getUniqueId());
    }
}
