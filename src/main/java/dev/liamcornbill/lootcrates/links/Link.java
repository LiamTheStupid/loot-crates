package dev.liamcornbill.lootcrates.links;

import dev.liamcornbill.liamlib.LocationSerializable;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class Link implements ConfigurationSerializable {
    private final LocationSerializable location;
    private final String crateId;

    public Link(Location location, String crateId) {
        this.location = new LocationSerializable(location);
        this.crateId = crateId;
    }

    public Link(LocationSerializable location, String crateId) {
        this.location = location;
        this.crateId = crateId;
    }

    public Link(Map<String, Object> map) {
        this.location = (LocationSerializable) map.get("location");
        this.crateId = (String) map.get("crateId");
    }

    public LocationSerializable getLocation() {
        return location;
    }

    public String getCrateId() {
        return crateId;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();

        map.put("location", location);
        map.put("crateId", crateId);

        return map;
    }
}
