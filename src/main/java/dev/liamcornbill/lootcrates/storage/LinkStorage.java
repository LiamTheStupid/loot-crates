package dev.liamcornbill.lootcrates.storage;

import com.google.gson.reflect.TypeToken;
import dev.liamcornbill.liamlib.LocationSerializable;
import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.links.Link;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class LinkStorage {

    public static HashMap<LocationSerializable, Link> load() {
        File file = new File(LootCrates.getInstance().getDataFolder() + "/links.json");
        StringBuilder builder = new StringBuilder();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                builder.append(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            return new HashMap<>();
        }

        return LootCrates.getGson().fromJson(builder.toString(), new TypeToken<HashMap<LocationSerializable, Link>>(){}.getType());
    }

    public static void save(HashMap<LocationSerializable, Link> links) {
        File file = new File(LootCrates.getInstance().getDataFolder() + "/links.json");

        try (FileWriter writer = new FileWriter(file)) {
            LootCrates.getGson().toJson(links, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
