package dev.liamcornbill.lootcrates.storage;

import com.google.gson.reflect.TypeToken;
import dev.liamcornbill.lootcrates.LootCrates;
import dev.liamcornbill.lootcrates.crates.Crate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

public class CrateStorage {

    public static HashMap<String, Crate> load() {
        File file = new File(LootCrates.getInstance().getDataFolder() + "/crates.json");
        StringBuilder builder = new StringBuilder();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                builder.append(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            return new HashMap<>();
        }

        return LootCrates.getGson().fromJson(builder.toString(), new TypeToken<HashMap<String, Crate>>(){}.getType());
    }

    public static void save(HashMap<String, Crate> crates) {
        File file = new File(LootCrates.getInstance().getDataFolder() + "/crates.json");

        try (FileWriter writer = new FileWriter(file)) {
            LootCrates.getGson().toJson(crates, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
